[TOC]

## Beginning Note
The admins do not currently believe this will be implemented into the main coreOS ignition, as it appears to have some setup requirements that can not be updated, and a lack of developement on the application side.

Other platforms like Mastodon and Activity pub are being looked into.

## Quick Start

Clone this repository.

`cp .env.template .env`  
(Or copy the .env file from your oci-servers clone, if you are using that one, too.)

Edit `.env` with your information.

`sh setup.sh`

Proceed to setting up the oci-https-proxy with a Misskey config.

## Detailed Setup

For any more information, please see the misskey docker repository: https://github.com/docker-library/docs/blob/master/misskey/README.md
Clone this repository:  
`git clone https://gitlab.com/CloudArtAdmins/oci-misskey.git` 

Go into the repository's folder and copy the `.env.template` file:  
`cp .env.template .env`

Fill in the variables with your corrisponding values.  

## Troubleshooting
In general when running the script for the first time, my recommended method of troubleshooting is to view the individual container's logs.  
The misskey container itself: `podman logs misskey`  
The misskey database container: `podman logs misskey-db`   

#### Misconfigured something on first run, so it isn't all automated...
I'd recommend just removing everything and trying again with the proper configurations:
```
podman pod stop misskey-pod
podman pod rm misskey-pod
podman volume rm misskey-files misskey-config
sh setup.sh
```

#### Containers exit after closing my session/loging out.

This is assuming you want to run the script and keep the containers running.  
The alternative, probably more secure solution is to use a systemd unit, but assuming you don't want to do that....

As of systemd 230, you need to change your the default value of logind's variable `KillUserProcesses` to `no`.  
If you have the template of systemd-logind's configurations, you may just need to remove a comment `#`.

Edit the config file: `vim /etc/systemd/logind.conf`  
Add: `KillUserProcesses=no`   
Restart the service to apply configs: `sudo systemctl restart systemd-logind`  

Source: https://lwn.net/Articles/690166/

