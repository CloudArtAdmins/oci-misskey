podman network create proxy-net 

podman pod create \
	--name pod-misskey \
	-p 3000:3000 \
	--network proxy-net 
	
podman run -d \
	--name misskey-db \
	--pod misskey \
	--volume misskey-db:/var/lib/postgresql/data \
	--env POSTGRES_PASSWORD=$MISSKEY_POSTGRES_PASSWORD \
	--env POSTGRES_USER=misskey \
	--env POSTGRES_DB=misskey \
	docker.io/library/postgres:alpine

podman run -d \
	--name misskey-redis \
	--pod misskey \
	--volume misskey-redis:/data:z \
	docker.io/library/redis:alpine
	
podman run -d \
	--name misskey \
	--pod misskey \
	--volume misskey-configs:/misskey/.config:ro,z \
	--volume misskey-files:/misskey/files:z \
	docker.io/misskey/misskey

